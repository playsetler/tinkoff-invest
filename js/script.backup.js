var allStocks = [];
/* Создаем канал */
var pusher = {};
var channel = {};
var least = 0;
var greatest = 0;
window.addEventListener('load', () => {
    SendRequest('server.php', {action: 'get-trade-status'}, (response) => {
        if(response.status == 'success') {
            pusher = new Pusher('0fbea55126d60ee64bfb', {
                cluster: "ap3"
            });
            channel = pusher.subscribe(response.name);
            var callback = (eventName, data) => {
                switch(eventName) {
                    case 'get-stock-value':
                        // Подписываемся на событие update-data-event чтобы получать рыночую цену акции в реальном времени
                        ParseData(data);
                        var history = JSON.parse(data.history) || '';
                        var elements = '';
                        least = history[0].price;
                        greatest = history[0].price;
                        var leastIndex = 0;
                        var greatestIndex = 0;
                        for (let i = 0; i < history.length; i++) {
                            if(least > history[i].price) {
                                leastIndex = i;
                                least = history[i].price;
                            }
                            if(greatest < history[i].price) {
                                greatestIndex = i;
                                greatest = history[i].price;
                            }
                            elements += '<div class="item"><div class="value">' + history[i].price + history[i].currency + '</div>';
                            elements += '<div class="time">' + history[i].time + '</div></div>';
                        }
                        document.querySelector('.history .items').innerHTML = elements;
                        document.querySelectorAll('.history .items .item')[leastIndex].style.backgroundColor = '#28E762';
                        document.querySelectorAll('.history .items .item')[greatestIndex].style.backgroundColor = '#f95d51';
                        break;
            
                    case 'restart-script-event':
                        // Поддерживаем канал для циклического перезапуска сервера
                        SendRequest('server.php', {action: 'get-stock-value'});
                        break;
            
                    default:
                        break;
                }
            };
            
            channel.bind_global(callback);
            SendRequest('server.php', {action: 'update'});
            SendRequest('server.php', {action: 'get-stock-value'});
        } else {
            document.querySelector('.history .items').textContent = 'Недоступно для торговли';
        }
        ParseData(response);
    });
    SendRequest('server.php', {action: 'get-stocks'}, (stocks) => {
        let input = document.querySelector('#ticker');

        input.addEventListener("focus", (e) => {
            e.preventDefault();
            input.setAttribute('data-focus', true);
        });
        input.addEventListener("focusout", (e) => {
            e.preventDefault();
            input.setAttribute('data-focus', false);
        });

        document.querySelector('#form-search-stock').addEventListener('keyup', (e) => {
            e.preventDefault();

            document.querySelector('#ticker-invalid').animate([
                {opacity: 0}
            ], {duration: 300, easing: 'ease-out', fill: 'both'});

            var listMatches = document.querySelector('.matches');
            function clearInput() {
                while (listMatches.firstChild) {
                    listMatches.removeChild(listMatches.firstChild);
                }
            }

            let inputValue = input.value.toUpperCase();
            if(inputValue.length < 1) {
                clearInput();
                return false;
            }
            
            clearInput();

            stocks.forEach((stock) => {
                allStocks.push(stock['ticker']);
                if (stock.ticker.includes(inputValue)) {
                    var li = document.createElement('li');
                    var a = document.createElement('a');
                    a.textContent = stock['ticker'];
                    a.setAttribute('href', '#');
                    a.addEventListener('click', (e) => {
                        e.preventDefault();
                        clearInput();
                        input.value = stock['ticker'];
                        SendRequest('server.php', {action: 'stock-select', ticker: stock['ticker']}, function(data) {
                            if(data.status == 'success') window.location.reload(false);
                        });
                    });
                    li.appendChild(a);
                    listMatches.appendChild(li);
                }
            });
        });
    });
});

function ParseData(data) {
    var input = document.querySelector('#ticker');
    if(!input.getAttribute('data-focus')) input.value = data.name;
    document.querySelector('.balance span').textContent = data.balance;
    document.querySelector('.price span').textContent = data.price;
    document.querySelector('.price .buy input').placeholder = data.price;
    document.querySelector('.price .sell input').placeholder = data.price;
    document.querySelector('.middle-price').textContent = data.middlePrice;
    document.querySelector('.total-price').textContent = data.totalPrice;
    document.querySelector('.dinamic').textContent = data.dinamic;
    document.querySelector('.count').textContent = data.count;
    document.querySelector('.available span').textContent = data.available;
    document.querySelector('.price .buy input').max = data.max;
    document.querySelector('.price .sell input').min = data.min;
}




/* Обработка ввода в поля */
var inputBuyPrice = document.querySelector('.price .buy input');
inputBuyPrice.addEventListener("focus", (e) => {
    e.preventDefault();
    inputBuyPrice.setAttribute('data-focus', true);
});
inputBuyPrice.addEventListener("focusout", (e) => {
    e.preventDefault();
    inputBuyPrice.setAttribute('data-focus', false);
});

var inputSellPrice = document.querySelector('.price .buy input');
inputSellPrice.addEventListener("focus", (e) => {
    e.preventDefault();
    inputSellPrice.setAttribute('data-focus', true);
});
inputSellPrice.addEventListener("focusout", (e) => {
    e.preventDefault();
    inputSellPrice.setAttribute('data-focus', false);
});





/* Обработка нажатия кнопок */
var buttons = document.querySelectorAll("button");
buttons.forEach((button) => {
    button.addEventListener('click', (e) => {
        e.preventDefault();
        button.setAttribute('disabled', true);
        switch(button.getAttribute('data-action')) {
            case 'login':
                SendRequest('server.php', {action: button.getAttribute('data-action')}, function(data) {
                    button.removeAttribute('disabled');
                });
                break;

            case 'buy':
                var inputPrice = document.querySelector('.price .buy input');
                SendRequest('server.php', {
                        action: button.getAttribute('data-action'),
                        price: inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                        count: inputCount.value == '' ? inputCount.placeholder : inputCount.value
                    },
                    function(data) {
                        button.removeAttribute('disabled');
                        ParseData(data);
                });
                break;

            case 'sell':
                var inputPrice = document.querySelector('.price .sell input');
                SendRequest('server.php', {
                        action: button.getAttribute('data-action'),
                        price: inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                        count: inputCount.value == '' ? inputCount.placeholder : inputCount.value
                    },
                    function(data) {
                        button.removeAttribute('disabled');
                        ParseData(data);
                });
                break;

            case 'update':
                SendRequest('server.php', {action: button.getAttribute('data-action')}, function(data) {
                    button.removeAttribute('disabled');
                });
                break;

            case 'stock-select':
                if(allStocks.includes(document.querySelector('#ticker').value)) {
                    SendRequest('server.php', {action: button.getAttribute('data-action'), fig: document.querySelector('#fig').value, ticker: document.querySelector('#ticker').value}, function(data) {
                        button.removeAttribute('disabled');
                        if(data.status == 'success') window.location.reload(false);
                        
                    });
                } else {
                    button.removeAttribute('disabled');
                    document.querySelector('#ticker-invalid').animate([
                        {opacity: 1}
                    ], {duration: 300, easing: 'ease-out', fill: 'both'});
                }
                break;

            default:
                break;
        }
    }, false);
});


function SendRequest(url, data, callback = false) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.withCredentials = true;
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    request.send(new URLSearchParams(data).toString());
    request.addEventListener("readystatechange", () => {
        if(request.readyState === 4) {
            if(request.status === 200) {
                if(callback && request.responseText != '') callback(JSON.parse(request.responseText) || '');
            }
        }
    });
}

function AnimateClick(type, element) {
    if(type == 1) {
        var animation = element.animate([
            {transform: 'scale(0.95)'}
        ], {duration: 0, easing: 'ease-out', delay: 0});
        animation.addEventListener('finish', () => {
            element.style.transform = 'scale(0.95)';
        });
    } else if(type == 0) {
        var animation = element.animate([
            {transform: 'scale(1)'}
        ], {duration: 200, easing: 'ease-out', delay: 0});
        animation.addEventListener('finish', () => {
            element.style.transform = 'scale(1)';
        });
    }
}

var animateElements = document.querySelectorAll(".animate-click");
animateElements.forEach((animateElement) => {
    animateElement.addEventListener('touchstart', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mousedown", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mousedown', (e) => {
        e.preventDefault();
        AnimateClick(1, animateElement)}, false);

    animateElement.addEventListener('touchend', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mouseup", { bubbles: true, cancelable: true }));
        e.target.dispatchEvent(new MouseEvent("click", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mouseup', (e) => {
        e.preventDefault();
        AnimateClick(0, animateElement)}, false);
        
    animateElement.addEventListener('touchcancel', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mouseout", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mouseout', (e) => {
        e.preventDefault();
        AnimateClick(0, animateElement)}, false);
});