var least = 0;
var greatest = 0;
var tinkoffClient;
window.addEventListener('DOMContentLoaded', () => {
    tinkoffClient = new TinkoffClient();
    
    tinkoffClient.on('update-data', (data) => {
        if(!data.stockMarket.status) {
            document.querySelector('.portfolio .operation').style.display = 'none';
        } else {
            document.querySelector('.portfolio .operation').style.display = 'flex';
        }
        ParseData(data);

        /* Если стоимость акции на рынке выше чем в портфеле окрашиваем кнопку продать в зеленый */
        if(Math.sign(data.stockPortfolio.ratio) == 1 && data.stockPortfolio.ratio != 0) {
            document.querySelector('.operation .sell button').style.backgroundColor = '#28E762';
        } else {
            document.querySelector('.operation .sell button').style.backgroundColor = '#e9ecef';
        }

        /* Разбираем историю цены акции */
        if(data.history != '') {
            var elements = '';
            least = data.history[0].price;
            greatest = data.history[0].price;
            var leastIndex = 0;
            var greatestIndex = 0;
            for (let i = 0; i < data.history.length; i++) {
                if(least > data.history[i].price) {
                    leastIndex = i;
                    least = data.history[i].price;
                }
                if(greatest < data.history[i].price) {
                    greatestIndex = i;
                    greatest = data.history[i].price;
                }
                elements += '<div class="item"><div class="value">' + data.history[i].price + '</div>';
                elements += '<div class="time">' + data.history[i].time + '</div></div>';
            }
            document.querySelector('.history .items').innerHTML = elements;
            document.querySelectorAll('.history .items .item')[leastIndex].style.backgroundColor = '#28E762';
            document.querySelectorAll('.history .items .item')[greatestIndex].style.backgroundColor = '#f95d51';
        }
        

        /* Разбираем заказы */
        var orders = '';
        var container = document.querySelector('.orders .items');
        
        if(!container.getAttribute('data-focus')) {
            container.innerHTML = '';
            data.orders.forEach((order) => {
                orders += '<div id="order'+ order.id +'" class="order row g-0 col-12" data-action="'+ order.type +'"><span class="type '+ order.type +' col-1">'+ (order.type == "buy" ? "<b>К</b>" : order.type == "sell" ? "<b>П</b>" : "") +'</span><div class="count input-group col"><input type="number" class="form-control" value="' + order.count + '" readonly><span class="input-group-text">шт.</span></div><div class="price input-group col"><input type="number" class="form-control" value="' + order.price + '" readonly><span class="input-group-text">$</span></div><div class="operation col-4 row g-0"><div class="edit col"><button class="animate-click" data-action="edit" onclick="orderEdit(this)" data-id="'+ order.id +'">ред.</button></div><div class="delete col-auto"><button class="animate-click" data-action="cancel" onclick="orderCancel(this)" data-id="'+ order.id +'">X</button></div></div></div>';
            });
            container.innerHTML = orders;
        }
    });

    tinkoffClient.getStocks((stocks) => {
        var input = document.querySelector('#ticker');
        var allStocks = [];

        input.addEventListener("focus", (e) => {
            e.preventDefault();
            input.setAttribute('data-focus', true);
        });
        input.addEventListener("focusout", (e) => {
            e.preventDefault();
            input.setAttribute('data-focus', false);
        });

        document.querySelector('#form-search-stock').addEventListener('keyup', (e) => {
            e.preventDefault();
            /* document.querySelector('#ticker-invalid').animate([
                {opacity: 0}
            ], {duration: 300, easing: 'ease-out', fill: 'both'}); */

            var listMatches = document.querySelector('.matches');
            function clearInput() {
                while (listMatches.firstChild) {
                    listMatches.removeChild(listMatches.firstChild);
                }
                document.querySelector('.matches-output').style.display = 'none';
            }

            var inputValue = input.value.toUpperCase();
            if(inputValue.length < 1) {
                clearInput();
                return false;
            }
            
            clearInput();

            stocks.forEach((stock) => {
                allStocks.push(stock['ticker']);
                if (stock.ticker.includes(inputValue)) {
                    var li = document.createElement('li');
                    var a = document.createElement('a');
                    a.textContent = stock['ticker'];
                    a.setAttribute('href', '#');
                    a.addEventListener('click', (e) => {
                        e.preventDefault();
                        clearInput();
                        input.value = stock['ticker'];
                        tinkoffClient.setStock(stock['ticker'], (result) => {
                            if(result) window.location.reload(false);
                        });
                    });
                    li.appendChild(a);
                    listMatches.appendChild(li);
                    document.querySelector('.matches-output').style.display = 'block';
                }
            });
        });
    });

});

function ParseData(data) {
    document.querySelector('#status').classList.remove('on', 'off');
    document.querySelector('#status').classList.add(data.stockMarket.status ? 'on' : 'off');
    var input = document.querySelector('#ticker');
    if(!input.getAttribute('data-focus')) input.value = data.stock.ticker;
    document.querySelector('.balance span').textContent = data.balance + data.stockMarket.currency;
    document.querySelector('.stock-info .name').textContent = data.stockMarket.name;
    document.querySelector('.stock-info .price .value').textContent = data.stockMarket.price + data.stockMarket.currency;
    document.querySelector('.price .buy input').placeholder = data.stockMarket.price;
    document.querySelector('.price .sell input').placeholder = data.stockMarket.price;
    document.querySelector('.price .buy .input-group-text').textContent = data.stockMarket.currency;
    document.querySelector('.price .sell .input-group-text').textContent = data.stockMarket.currency;
    document.querySelector('.middle-price').textContent = data.stockPortfolio.price + data.stockPortfolio.currency;
    document.querySelector('.total-price').textContent = data.stockPortfolio.totalPrice + data.stockPortfolio.currency;
    var ratio = document.querySelector('.stock-info .price .ratio');
    ratio.classList.remove('negative', 'positive');
    ratio.textContent = data.stockPortfolio.ratio + '%';
    if(Math.sign(data.stockPortfolio.ratio) == -1) ratio.classList.add('negative');
    if(Math.sign(data.stockPortfolio.ratio) == 1) ratio.classList.add('positive');
    document.querySelector('.count').textContent = data.stockPortfolio.count + ' шт.';
    document.querySelector('.available').textContent = data.stockPortfolio.available + ' шт.';
    document.querySelector('.price .buy input').max = data.stockMarket.max;
    document.querySelector('.price .sell input').min = data.stockMarket.min;
}




/* Обработка ввода в поля */
var inputBuyPrice = document.querySelector('.price .buy input');
inputBuyPrice.addEventListener("focus", (e) => {
    e.preventDefault();
    inputBuyPrice.setAttribute('data-focus', true);
});
inputBuyPrice.addEventListener("focusout", (e) => {
    e.preventDefault();
    inputBuyPrice.setAttribute('data-focus', false);
});

var inputSellPrice = document.querySelector('.price .buy input');
inputSellPrice.addEventListener("focus", (e) => {
    e.preventDefault();
    inputSellPrice.setAttribute('data-focus', true);
});
inputSellPrice.addEventListener("focusout", (e) => {
    e.preventDefault();
    inputSellPrice.setAttribute('data-focus', false);
});





/* Обработка нажатия кнопок */
var buttons = document.querySelectorAll("button");
buttons.forEach((button) => {
    button.addEventListener('click', (e) => {
        e.preventDefault();
        button.setAttribute('disabled', true);
        button.classList.add('load');
        switch(button.getAttribute('data-action')) {
            case 'login':
                SendRequest('server.php', {action: button.getAttribute('data-action')}, function(data) {
                    button.removeAttribute('disabled');
                    button.classList.remove('load');
                });
                break;

            case 'buy': // покупка акции
                var inputPrice = document.querySelector('.operation .price .buy input');
                var inputCount = document.querySelector('.operation .count .buy input');
                tinkoffClient.buy(
                    inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                    inputCount.value == '' ? inputCount.placeholder : inputCount.value,
                    (data) => {
                        button.removeAttribute('disabled');
                        button.classList.remove('load');
                        ParseData(data);
                    });
                break;

            case 'sell': // продажа акции
                var inputPrice = document.querySelector('.operation .price .sell input');
                var inputCount = document.querySelector('.operation .count .sell input');
                tinkoffClient.sell(
                    inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                    inputCount.value == '' ? inputCount.placeholder : inputCount.value,
                    (data) => {
                        button.removeAttribute('disabled');
                        button.classList.remove('load');
                        ParseData(data);
                    });
                break;

            case 'stock-select': // выбор акции
                if(allStocks.includes(document.querySelector('#ticker').value)) {
                    SendRequest('server.php', {action: button.getAttribute('data-action'), fig: document.querySelector('#fig').value, ticker: document.querySelector('#ticker').value}, function(data) {
                        button.removeAttribute('disabled');
                        button.classList.remove('load');
                        if(data.status == 'success') window.location.reload(false);
                        
                    });
                } else {
                    button.removeAttribute('disabled');
                    button.classList.remove('load');
                    document.querySelector('#ticker-invalid').animate([
                        {opacity: 1}
                    ], {duration: 300, easing: 'ease-out', fill: 'both'});
                }
                break;

            default:
                break;
        }
    }, false);
});

var oldPrice = '';
var oldCount = '';

/* Редактирование заявки */
function orderEdit(button) {
    var order = document.querySelector('#order' + button.getAttribute('data-id'));
    var typeAction = order.getAttribute('data-action');
    var container = document.querySelector('.orders .items');
    container.setAttribute('data-focus', true);
    var inputCount = order.querySelector('.count input[type=number]');
    var inputPrice = order.querySelector('.price input[type=number]');
    
    if(!inputCount.hasAttribute('readonly') && !inputPrice.hasAttribute('readonly')) {
        inputCount.setAttribute('readonly', true);
        inputPrice.setAttribute('readonly', true);
        if(oldPrice != inputPrice.value || oldCount != inputCount.value) {
            
            /* tinkoffClient.cancelOrder(button.getAttribute('data-id'), () => {
                tinkoffClient.buy(
                    inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                    inputCount.value == '' ? inputCount.placeholder : inputCount.value,
                    (data) => {
                        container.removeAttribute('data-focus');
                        button.removeAttribute('disabled');
                        ParseData(data);
                    }
                );
            }); */
            console.log(inputPrice.value + ' ' + inputCount.value);
            tinkoffClient.editOrder(
                typeAction,
                button.getAttribute('data-id'),
                inputCount.value == '' ? inputCount.placeholder : inputCount.value,
                inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value,
                (data) => {
                    container.removeAttribute('data-focus');
                    button.removeAttribute('disabled');
                    ParseData(data);
            });
            Loader('.orders .items');
        }
    } else {
        oldPrice = inputPrice.value == '' ? inputPrice.placeholder : inputPrice.value;
        oldCount = inputCount.value == '' ? inputCount.placeholder : inputCount.value;
        inputCount.removeAttribute('readonly');
        inputPrice.removeAttribute('readonly');
    }
}

/* Отмена заявки */
function orderCancel(button) {
    tinkoffClient.cancelOrder(button.getAttribute('data-id'),
    (data) => {
        button.removeAttribute('disabled');
        ParseData(data);
    });
}

/* Обработка изменения поля цены покупки */
function inputPriceBuyChange(input) {
    if (parseFloat(input.value) > input.max) {
        input.style.borderColor = '#FF0000';
        document.querySelector('.action .buy button').setAttribute('disabled', true);
    } else {
        input.style.borderColor = '#ced4da';
        document.querySelector('.action .buy button').removeAttribute('disabled');
    }
}

/* Обработка изменения поля цены продажи */
function inputPriceSellChange(input) {
    if (parseFloat(input.value) < input.min) {
        input.style.borderColor = '#FF0000';
        document.querySelector('.action .sell button').setAttribute('disabled', true);
    } else {
        input.style.borderColor = '#ced4da';
        document.querySelector('.action .sell button').removeAttribute('disabled');
    }
}