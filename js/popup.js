class Popup {
    form = {};
    fields = {};
    message = false;
    loader = {};
    buttonSend = {};
    buttonCancel = {};
    messageTimer = "";
    open = false;
    param = {};
    callback = () => {};
  
    constructor(param) {
        this.param = param;
        this.loader = document.createElement("div");
        this.loader.innerHTML = '<div class="dot-loader"></div><div class="dot-loader"></div><div class="dot-loader"></div>';
        this.loader.classList.add('loader');
        var form = document.createElement("form");
        form.id = "feedback-form";
        form.method = "POST";
        form.classList.add('feedback-form');
        var header = '<span id="form-close" class="animate-click"></span><div class="header">' + param.header;
        form.innerHTML = header;
  
        var form_ul = document.createElement("ul");
        form_ul.classList.add('flex-outer');
  
        var content = document.createElement("p");
          content.textContent = param.content;
          form_ul.appendChild(content);
  
        if(param.fields) {
          param.fields.forEach((item) => {
              var input = {};
              input = item.type == 'textarea' ? document.createElement("textarea") : document.createElement("input");
              if(item.type == 'text' || item.type == 'email' || item.type == 'textarea') {
                  input.setAttribute('type', 'text');
                  input.setAttribute('maxlength', item.max);
              } else if(item.type == 'number') {
                  input.setAttribute('type', 'number');
                  input.setAttribute('max', item.max);
              } else if(item.type == 'phone') {
                  input.setAttribute('type', 'tel');
              } else if(item.type == 'password') {
                  input.setAttribute('type', 'password');
              }
              input.setAttribute('name', item.id);
              input.id = item.id;
              if(item.value) input.value = item.value;
              if(item.required) input.setAttribute('required', true);
              if(item.placeholder) input.setAttribute('placeholder', item.placeholder);
              input.addEventListener('input', (event) => {
                  if(event.target.value != "") {
                      this.messageBox(false);
                  }
              });
    
              var li = document.createElement("li");
              li.innerHTML = '<div class="flex-label"><label for="' + item.id + '">' + item.label + '</label></div><div class="flex-input"></div>';
              li.querySelector(".flex-input").appendChild(input);
              form_ul.appendChild(li);
          });
        }
        
        var div = document.createElement("div");
        div.id = 'feedback-message';
        var li = document.createElement("li");
        li.appendChild(div);
        form_ul.appendChild(li);
        form.appendChild(form_ul);
  
        // создаем кнопку отправки формы
        this.buttonSend = document.createElement("button");
        this.buttonSend.innerText = param.buttonSend;
        this.buttonSend.classList.add('animate-click');
        this.buttonSend.addEventListener('click', (event) => {
            event.preventDefault();
            var data = {action: param.arguments.action, id: param.arguments.id, parent: param.arguments.parent, name: '', description: ''};
            var error = new Array();
            for (var i = 0, element; element = form.elements[i++];) {
                if (element.value != "") {
                    this.fields[element.id] = element.value;
                    var index = error.indexOf(element.id);
                    if(index != -1) error.splice(index, 1);
                    element.style.borderColor = "#28E762";
                } else if(element.required) {
                    error.push(element.id);
                    element.style.borderColor = "#f95d51";
                }
            }
            
            if(error.length <= 0) {
                Object.assign(data, this.fields);
                this.send(data);
            } else {
                this.messageBox(true, "error", "заполните обязательные поля, обведенные красным цветом");
            }
        });
        var li = document.createElement("li");
        li.appendChild(this.buttonSend);
        form_ul.appendChild(li);
        form.appendChild(form_ul);
  
        var formContainer = document.createElement("div");
        formContainer.classList.add('form-container');
        formContainer.appendChild(form);
        this.form = form;
        document.body.appendChild(formContainer);
  
        var animateElements = document.querySelectorAll(".animate-click");
        animateElements.forEach((animateElement) => {
            animateElement.addEventListener('touchstart', (e) => {
                e.preventDefault();
                e.target.dispatchEvent(new MouseEvent("mousedown", { bubbles: true, cancelable: true }));
            }, false);
            animateElement.addEventListener('mousedown', (e) => {
                e.preventDefault();
                this.animateClick(1, animateElement)}, false);
  
            animateElement.addEventListener('touchend', (e) => {
                e.preventDefault();
                e.target.dispatchEvent(new MouseEvent("mouseup", { bubbles: true, cancelable: true }));
                e.target.dispatchEvent(new MouseEvent("click", { bubbles: true, cancelable: true }));
            }, false);
            animateElement.addEventListener('mouseup', (e) => {
                e.preventDefault();
                this.animateClick(0, animateElement)}, false);
                
            animateElement.addEventListener('touchcancel', (e) => {
                e.preventDefault();
                e.target.dispatchEvent(new MouseEvent("mouseout", { bubbles: true, cancelable: true }));
            }, false);
            animateElement.addEventListener('mouseout', (e) => {
                e.preventDefault();
                this.animateClick(0, animateElement)}, false);
        });
  
        document.querySelector("#form-close").addEventListener('click', () => { this.openForm(false); });
    }
  
    loading(on = false) {
        if(on) {
            this.buttonSend.textContent = "";
            this.buttonSend.appendChild(this.loader);
            this.buttonSend.disabled = true;
        } else {
            this.buttonSend.innerHTML = "";
            this.buttonSend.innerText = this.param.buttonSend;
            this.buttonSend.disabled = false;
            //this.openForm(false);
        }
    }
  
    clearForm() {
        for (var i = 0, element; element = this.form.elements[i++];) {
            element.value = "";
            element.style.borderColor = "#c9c9c9";
        }
    }
  
    send(data) {
        this.loading(true);
        var request = new XMLHttpRequest();
        request.open('POST', 'classes/controller.php', true);
        request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        request.send(JSON.stringify(data));
        request.addEventListener("readystatechange", () => {
            if(request.readyState === 4) {
                if(request.status === 200) {
                  this.messageBox(true, "notice", "Успешно");
                  this.callback(request.responseText);
                } else {
                    this.messageBox(true, "error", "Ошибка отправки формы: " + request.statusText);
                    console.log(request.responseText);
                }
                this.loading(false);
            }
        });
    }
  
    messageBox(on, type = "", message = "") {
        var messageBox = document.querySelector("#feedback-message");
        if(on) {
            if(type == "error") {
                messageBox.style.background = "#f95d51";
            } else if(type == "notice") {
                messageBox.style.background = "#28E762";
            }
            messageBox.innerText = message;
            if(!this.message && this.message != type) {
                this.animateVisibility(1, messageBox);
                this.message = type;
            }
  
            clearTimeout(this.messageTimer);
            this.messageTimer = setTimeout(() => { this.animateVisibility(0, messageBox); this.message = false; }, 3000);
        } else {
            clearTimeout(this.messageTimer);
            this.animateVisibility(0, messageBox);
            this.message = false;
        }
        
    }
  
    openForm(on) {
        if(on && !this.open) {
            document.querySelector(".form-container").style.display = "block";
            this.animateTransform(1, {transform: ['translate(0px, -300px)', 'translate(0px, 0px)'], opacity: [0, 1]}, document.querySelector("#feedback-form"));
            this.open = true;
        } else {
            this.animateTransform(0, {transform: ['translate(0px, 0px)', 'translate(0px, -300px)'], opacity: [1, 0]}, document.querySelector("#feedback-form"), () => {document.querySelector(".form-container").style.display = "none";document.querySelector(".form-container").remove();});
            this.open = false;
            
        }
    }
  
    animateClick(type, element) {
        //document.getAnimations().forEach((animation) => { animation.cancel(); });
        if(type == 1) {
            var animation = element.animate([
                {transform: 'scale(0.95)'}
            ], {duration: 0, easing: 'ease-out', delay: 0});
            animation.addEventListener('finish', () => {
                element.style.transform = 'scale(0.95)';
            });
        } else if(type == 0) {
            var animation = element.animate([
                {transform: 'scale(1)'}
            ], {duration: 200, easing: 'ease-out', delay: 0});
            animation.addEventListener('finish', () => {
                element.style.transform = 'scale(1)';
            });
        }
    }
  
    animateVisibility(type, element) {
        if(type == 0) {
            var animation = element.animate([
                {opacity: 0}
            ], {duration: 300, easing: 'ease-out', delay: 0});
            animation.addEventListener('finish', () => {
                element.style.display = "none";
            });
        } else if(type == 1) {
            element.style.display = "block";
            var animation = element.animate([
                {opacity: 1}
            ], {duration: 0, easing: 'ease-out', delay: 0});
        }
    }
  
    animateTransform(type, anim, element, callback) {
        if(type == 1) {
            var animation = element.animate(anim, {duration: 250, easing: 'cubic-bezier(0, 0, 0.58, 1)', delay: 0, fill:"forwards"});
        } else if(type == 0) {
            var animation = element.animate(anim, {duration: 250, easing: 'cubic-bezier(0.42, 0, 1, 1)', delay: 0, fill:"forwards"});
            animation.addEventListener('finish', () => {
                callback();
            });
        }
    }
  }