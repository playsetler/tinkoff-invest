function Loader(container) {
    document.querySelector(container).innerHTML = '<div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>';
}


function SendRequest(url, data, callback = false) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.withCredentials = true;
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    request.send(new URLSearchParams(data).toString());
    request.addEventListener("readystatechange", () => {
        if(request.readyState === 4 && request.status === 200 && request.responseText != '') {
            if(callback) {
                try {
                    var response = JSON.parse(request.responseText);
                    if(response.status == 'error') {
                        console.log(response.message);
                    }
                    callback(response);
                } catch(e) {
                    console.log(e);
                }
            } 
        }
    });
}

function AnimateClick(type, element) {
    if(type == 1) {
        var animation = element.animate([
            {transform: 'scale(0.95)'}
        ], {duration: 0, easing: 'ease-out', delay: 0});
        animation.addEventListener('finish', () => {
            element.style.transform = 'scale(0.95)';
        });
    } else if(type == 0) {
        var animation = element.animate([
            {transform: 'scale(1)'}
        ], {duration: 200, easing: 'ease-out', delay: 0});
        animation.addEventListener('finish', () => {
            element.style.transform = 'scale(1)';
        });
    }
}

var animateElements = document.querySelectorAll(".animate-click");
animateElements.forEach((animateElement) => {
    animateElement.addEventListener('touchstart', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mousedown", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mousedown', (e) => {
        e.preventDefault();
        AnimateClick(1, animateElement)}, false);

    animateElement.addEventListener('touchend', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mouseup", { bubbles: true, cancelable: true }));
        e.target.dispatchEvent(new MouseEvent("click", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mouseup', (e) => {
        e.preventDefault();
        AnimateClick(0, animateElement)}, false);
        
    animateElement.addEventListener('touchcancel', (e) => {
        e.preventDefault();
        e.target.dispatchEvent(new MouseEvent("mouseout", { bubbles: true, cancelable: true }));
    }, false);
    animateElement.addEventListener('mouseout', (e) => {
        e.preventDefault();
        AnimateClick(0, animateElement)}, false);
});