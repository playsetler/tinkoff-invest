class TinkoffClient {
    #pusher = {};
    #channel = {};
    #stocks = [];
    #stock = {};
    #queue = 0; // очередь событий чтобы старые данные не затирали новые
    messages = []; // история сообщений уведомлений или ошибок
    loader = {};
    #params = {
        pusher: {
            appKey: '0fbea55126d60ee64bfb',
            cluster: 'ap3',
            channelName: ''
        }

    };
    #events = new Map(); // словарь событий класса
    #typeEvents = ['start-init', 'end-init', 'receive', 'update-data', 'buyed', 'selled']; // типы событий класса
    #callback = (eventName, data) => {
        switch(eventName) {
            case 'update-data':
                // Подписываемся на событие update-data чтобы получать информацию в реальном времени
                if(data.queue > this.#queue) {
                    this.#queue = data.queue;
                    
                }
                this.#dispatch('update-data', data);
                this.#stock = data.stockMarket;console.log(data);
                break;
    
            case 'restart':
                // Поддерживаем канал для циклического перезапуска сервера
                this.#request({action: 'update-data'});
                break;
    
            default:
                break;
        }
    }
  

    constructor(params = {}) {
        this.#dispatch('start-init');
        this.#params = Object.assign(this.#params, params);
        this.getStocks();
        this.#connect(() => { this.#dispatch('end-init'); });
    }

    /* Подключение к серверу */
    #connect(callback = false) {
        this.#request({action: 'connect'}, (response) => {
            this.#pusher = new Pusher(this.#params.pusher.appKey, {
                cluster: this.#params.pusher.cluster
            });
            this.setChannel(response.data.channel);
            if(callback) callback();
        });
    }


    /* Подключаемся к необходимому каналу */
    setChannel(name = false) {
        this.#channel = this.#pusher.subscribe(name ? name : this.#params.channelName);
        this.#channel.bind_global(this.#callback);
        this.#params.channelName = name;
        this.#request({action: 'update-data'});
    }

    /* подписка на события за пределами класса */
    on(key, callback) {
        if(this.#typeEvents.indexOf(key) !== -1 && !this.#events.has(key)) this.#events.set(key, callback);
    }
    

    /* вызов событий класса */
    #dispatch(key, params) {
        if(this.#events.has(key)) {
            let callback = this.#events.get(key);
            callback(params);
        }
    }


    /* AJAX-запрос */
    #request(data, callback = false, url = 'server.php', async = true) {
        var request = new XMLHttpRequest();
        request.open('POST', url, async);
        request.withCredentials = true;
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        request.send(new URLSearchParams(data).toString());
        request.addEventListener("readystatechange", () => {
            if(request.readyState === 4 && request.status === 200 && request.responseText != '') {console.log(request.responseText);
                var response = JSON.parse(request.responseText);
                if(callback) {
                    try {
                        if(response.data ?? false) {
                            if(response.data.queue && response.data.queue > this.#queue) {
                                this.#queue = response.data.queue;
                            }
                        }
                        callback(response);
                    } catch(e) {
                        this.message('error', e);
                    }
                }
                if(response.status == 'error') {
                    this.message('error', response.message);
                }
            }
        });
    }


    /* Показать уведомление с сообщением */
    message(type, text) {
        switch(type) {
            case 'notice':
                alert(text);
                break;

            case 'error':
                alert(text);
                break;

            default:
                alert(text);
                break;
        }
        this.messages.push({ type: type, text: text });
    }


    /* Установить акцию для наблюдения */
    setStock(ticker, callback = false) {
        this.#request({action: 'set-stock', ticker: ticker}, (response) => {
            if(response.status == 'success') {
                if(callback) callback(true);
                this.setChannel(ticker);
            }
        });
    }


    /* Получить информацию об акции */
    getStock(ticker = '') {
        if(Object.keys(this.#stock).length == 0) {
            this.#request({action: 'get-stock', ticker: ticker}, (response) => {
                if(response.status == 'success') this.#stock = response.data;
            });
        }
    }


    /* Получить все акции */
    getStocks(callback = false) {
        if(this.#stocks.length <= 0) {
            this.#request({action: 'get-stocks'}, (response) => {
                if(response.status == 'success') {
                    this.#stocks = response.data;
                    if(callback) { callback(this.#stocks); } 
                    else { return this.#stocks; }
                }
            });
        } else {
            if(callback) { callback(this.#stocks); } 
            else { return this.#stocks; }
        }
    }


    /* Купить текущую акцию */
    buy(price, count = 1, callback = false) {
        this.#request({action: 'buy', price: price, count: count}, (response) => {
            if(response.status == 'success') {
                this.#stock = response.data;
            }
            if(callback) callback(response.data);
        }, 'server.php', true);
    }


    /* Продать текущую акцию */
    sell(price, count = 1, callback = false) {
        this.#request({action: 'sell', price: price, count: count}, (response) => {
            if(response.status == 'success') {
                this.#stock = response.data;
            }
            if(callback) callback(response.data);
        }, 'server.php', true);
    }


    /* Отменить заказ*/
    cancelOrder(orderId, callback = false) {
        this.#request({action: 'cancel-order', orderId: orderId}, (response) => {
            if(response.status == 'success') {
                if(callback) callback(response.data);
            }
        });
    }

    /* Редактировать заказ*/
    editOrder(typeAction, orderId, newCount, newPrice, callback = false) {
        this.#request({action: 'edit-order', orderId: orderId, typeAction: typeAction, count: newCount, price: newPrice}, (response) => {
            if(response.status == 'success') {
                if(callback) callback(response.data);
            }
        });
    }
}