<?php

ini_set('error_reporting', true);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

include_once(dirname(__FILE__) . '/autoloader.php');
require __DIR__ . '/vendor/autoload.php';

use jamesRUS52\TinkoffInvest\TIAccount;
use \PHPUnit\Framework\TestCase;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TISiteEnum;
use jamesRUS52\TinkoffInvest\TICurrencyEnum;
use jamesRUS52\TinkoffInvest\TIInstrument;
use jamesRUS52\TinkoffInvest\TIPortfolio;
use jamesRUS52\TinkoffInvest\TIOrder;
use jamesRUS52\TinkoffInvest\TIOperationEnum;
use jamesRUS52\TinkoffInvest\TIIntervalEnum;

date_default_timezone_set("Europe/Moscow");
$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);
$settings = json_decode($USER -> settings);
$pusher = new Pusher\Pusher(
    '0fbea55126d60ee64bfb',
    '1ffe5ee41d6def82ca20',
    '1267579',
    array(
        'cluster' => 'ap3'
    )
);
$client = '';
if($settings-> mode == 'exchange') {
    $client = new TIClient($settings-> exchange_key, TISiteEnum::EXCHANGE);
} elseif($settings-> mode == 'sandbox') {
    $client = new TIClient($settings-> sandbox_key, TISiteEnum::SANDBOX);
}
if(!$client) exit('Не установлены, либо не валидные токены');
$portfolio = $client -> getPortfolio();
$client -> setIgnoreSslPeerVerification(true);
//$client -> sbRegister();
$from = new \DateTime();
$from -> sub(new \DateInterval("P7D"));
$to = new \DateTime();


/* Получаем историю цены акции */
function getStockPriceHistory($db, $client, $ticker, $count = 20, $forced = false) {
    $result = $db -> select('history', '*', 'ticker="'. $ticker .'"', 'id DESC LIMIT ' . $count);
    $historyItems = array();
    while($historyItem = mysqli_fetch_assoc($result)) {
        $historyItems[] = array(
            'ticker' => $historyItem['ticker'],
            'currency' => $historyItem['currency'],
            'price' => $historyItem['price'],
            'time' => date('H:i:s', strtotime($historyItem['time']))
        );
    }

    // Если кол-во записей в базе данных превысило 500 записей, то удаляем одну старую запись
    $countItems = $db -> count('history');
    if(!$forced && $countItems > 500) $db -> query('DELETE FROM history ORDER BY id ASC LIMIT 1');
    return $historyItems;
}


/* Добавляем запись в базу данных с информацией о стоимости акции */
function addStockToDB($db, $stock) {
    $result = $db -> select('history', '*', false, 'id DESC LIMIT 1');
    $lastItem = array();
    if($result) $lastItem = mysqli_fetch_assoc($result);
    if($lastItem['price'] == $stock['price']) return false; // Если новое значение равно последнему значению из базы, то не сохраняем новое значение
    // Вставляем запись со стоимостью акции в базу данных
    $result = $db-> insert('history', array(
                                            'ticker' => $stock['ticker'],
                                            'currency' => $stock['currency'],
                                            'price' => $stock['price']));
    return $result;
}


/* Получаем рыночную цену акции по фиги */
function getStockMarket($settings, $client, $figi) {
    $information = array();
    $orderBook = $client -> getHistoryOrderBook($figi);
    $info = $client-> getInstrumentByTicker($settings -> TICKER);
    $currency = $info-> getCurrency();
    if($currency == 'RUB') $currency = 'P';
    if($currency == 'USD') $currency = '$';
    $information['status'] = $orderBook-> getTradeStatus() == 'NormalTrading' ? true : false; // статус торговли
    $information['price'] = $orderBook-> getLastPrice(); // текущая цена акции на рынке
    $information['min'] = $orderBook -> getLimitDown(); // минимальная цена акции
    $information['max'] = $orderBook -> getLimitUp(); // максимальная цена акции
    $information['currency'] = $currency;
    $information['name'] = $info-> getName();
    return $information;
}


/* Получаем акцию из портфеля по названию тикера */
function getStockPortfolio($client, $ticker) {
    $portfolio = $client -> getPortfolio();
    $stocks = $portfolio -> getAllinstruments();
    $stock = false;
    foreach($stocks as $key => $value) {
        if ($value-> getTicker() == $ticker) {
            $stock = $stocks[$key];
            break;
        }
    }

    return $stock;
}


/* Получаем из портфеля информацию об акции */
function getStockInfoPortfolio($portfolio, $stock, $marketPrice) {
    $information = array();
    $currency = $stock-> getAveragePositionPrice()-> currency;
    if($currency == 'RUB') $currency = 'P';
    if($currency == 'USD') $currency = '$';
    $price = $stock-> getAveragePositionPrice()-> value;
    $information['ticker'] = $stock-> getTicker();
    $information['figi'] = $stock-> getFigi();
    $information['name'] = $stock-> getName();
    $information['count'] = $stock-> getLots(); // кол-во акций в портфеле
    $information['price'] = number_format($price, 2, '.', ''); // цена акции в портфеле
    $information['currency'] = $currency; // валюта акции
    $information['totalPrice'] = number_format($price * $stock-> getLots(), 2, '.', ''); // стоимость всех акций в портфеле
    $information['ratio'] = number_format(($marketPrice / $price - 1) * 100, 2, '.', ''); // процентное соотношение рыночной цены акции к цене в портфеле
    $information['available'] = floor($portfolio -> getCurrencyBalance($currency) / $marketPrice); // кол-во акций, которое еще можно приобрести исходя из текущего баланса
    return $information;
}


/* Получаем список заказов */
function getOrders() {
    GLOBAL $client, $settings;
    $orders = array();
    $ordersAll = $client -> getOrders();
    foreach($ordersAll as $order) {
        if ($order-> getFigi() == $settings -> FIG) {
            $orders[] = array('id' => $order-> getOrderId(),
                                'type' => strtolower($order-> getOperation()),
                                'count' => $order-> getRequestedLots(),
                                'price' => $order-> getPrice());
        }
    }
    return $orders;
}


/* Получаем список наименований всех акций */
function getStocks() {
    GLOBAL $client;
    $stockes = $client -> getStocks();
    $tickers = array();
    foreach($stockes as $stock) {
        $tickers[] = array('ticker' => $stock-> getTicker(), 'fig' => $stock -> getFigi());
    }
    return $tickers;
}


/* Подготовка всех данных для отправки клиенту */
function createResponse($DB, $client, $portfolio, $settings) {
    $stockMarket = getStockMarket($settings, $client, $settings -> FIG);
    $stockMarket['ticker'] = $settings -> TICKER;
    $stockInstrument = getStockPortfolio($client, $settings -> TICKER);
    $stockPortfolio = array(
        'name' => '',
        'count' => 0,
        'price' => 0.00,
        'currency' => '',
        'totalPrice' => 0.00,
        'ratio' => 0.00,
        'available' => 0
    );
    if($stockInstrument) {
        $stockPortfolio = getStockInfoPortfolio($portfolio, $stockInstrument, $stockMarket['price']);
    }
    $orders = getOrders();
    $result = addStockToDB($DB, $stockMarket);
    $history = array();
    $history = getStockPriceHistory($DB, $client, $settings -> TICKER);
    $data = array(
                    'balance' => $portfolio -> getCurrencyBalance('RUB'),
                    'stock' => array('ticker' => $settings -> TICKER, 'figi' => $settings -> FIG),
                    'stockPortfolio' => $stockPortfolio,
                    'stockMarket' => $stockMarket,
                    'history' => $history,
                    'orders' => $orders);
    return $data;
}


/* Добавляем очередь данным чтобы старые данные не затирали новые */
function formQueueData($data) {
    $data['queue'] = microtime(true);
    return $data;
}


switch ($_REQUEST['action']) {
    case 'login':
        // будущая авторизация на стороне сервера
        //echo json_encode($_COOKIE['SESSID']);
        header('Location:'. dirname(explode('?', ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])[0]) .'/index.php');
        break;

    case 'connect':
        // подключение клиента
        $channel = strval(mt_rand());
        $newSettings = array_merge((array)$settings, array('channel' => $channel));
        $result = $DB -> update('users', array('settings' => json_encode($newSettings)), 'login = "'. $USER -> login .'"');
        if($result) echo json_encode(array('status' => 'success', 'data' => array('channel' => $channel)));
        break;

    case 'get-stocks':
        // отправить список названий всех тикеров
        echo json_encode(array('status' => 'success', 'data' => getStocks()));
        break;

    case 'set-stock':
        // выбрать акцию для наблюдения
        $stockes = getStocks();
        $matchKey = false;
        foreach($stockes as $key => $value) {
            if($value['ticker'] == $_REQUEST['ticker']) {
                $matchKey = $key;
                break;
            }
        }

        if ($matchKey) {
            $newSettings = array_merge((array)$settings, array('FIG' => $stockes[$matchKey]['fig'], 'TICKER' => $_REQUEST['ticker']));
            $result = $DB -> update('users', array('settings' => json_encode($newSettings)), 'login = "'. $USER -> login .'"');
            if($result) echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error', 'message' => 'Данная акция отсутствует в списке текущего брокера'));
        }
        
        break;

    case 'buy':
        try {
            $client -> sendOrder($settings -> FIG, (int)$_REQUEST['count'], TIOperationEnum::BUY, (float)$_REQUEST['price']);
            echo json_encode(array('status' => 'success', 'data' => formQueueData(createResponse($DB, $client, $portfolio, $settings))));
        } catch(Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $e-> getMessage(), 'data' => createResponse($DB, $client, $portfolio, $settings)));
        }
        break;

    case 'sell':
        try {
            $client -> sendOrder($settings -> FIG, (int)$_REQUEST['count'], TIOperationEnum::SELL, (float)$_REQUEST['price']);
            echo json_encode(array('status' => 'success', 'data' => formQueueData(createResponse($DB, $client, $portfolio, $settings))));
        } catch(Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $e-> getMessage(), 'data' => createResponse($DB, $client, $portfolio, $settings)));
        }
        break;

    case 'update-data':
        $i = 0;
        $lastData = '';
        while ($i < 20) {
            $data = createResponse($DB, $client, $portfolio, $settings);
            $pusher -> trigger($settings -> channel, 'update-data', formQueueData($data));
            
            $i++;
            sleep(0.7);
        }
        $pusher -> trigger($settings -> channel, 'restart', true);
        break;

    case 'cancel-order':
        try {
            $client -> cancelOrder($_REQUEST['orderId']);
            echo json_encode(array('status' => 'success', 'data' => formQueueData(createResponse($DB, $client, $portfolio, $settings))));
        } catch(Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $e-> getMessage()));
        }
        break;

    case 'edit-order':
        try {
            $status = $client -> cancelOrder($_REQUEST['orderId']);
            if($status = 'Ok') {
                $order = $client -> sendOrder($settings -> FIG, (int)$_REQUEST['count'], TIOperationEnum::getOperation(ucfirst($_REQUEST['typeAction'])), (float)$_REQUEST['price']);
                echo json_encode(array('status' => 'success', 'data' => formQueueData(createResponse($DB, $client, $portfolio, $settings)), 'info' => $settings -> FIG . '/' . (int)$_REQUEST['count'] . '/' . ucfirst($_REQUEST['typeAction']) . '/' . (float)$_REQUEST['price']));
            }
        } catch(Exception $e) {
            echo json_encode(array('status' => 'error', 'message' => $e-> getMessage()));
        }
        break;

    case 'save-token':
        $newSettings = array_merge((array)$settings, array('exchange_key' => $_REQUEST['exchange'], 'sandbox_key' => $_REQUEST['sandbox'], 'mode' => $_REQUEST['mode']));
        $result = $DB -> update('users', array('settings' => json_encode($newSettings)), 'login = "'. $USER -> login .'"');
        if($result) {
            header('Location:'. dirname(explode('?', ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])[0]) .'/account.php');
        }
        
        break;
}


/* Будущий шифратор ключей */
function cript($value) {
    $length = 88 - strlen($value) % 88;
    $pkcs7_pad = $value . str_repeat(chr($length), $length);

    $enc_name = openssl_encrypt(
        $pkcs7_pad, // заполненные данные
        'AES-256-CBC', // шифр и режим
        openssl_random_pseudo_bytes(32), // секретный ключ
        0, // опции (не используются)
        openssl_random_pseudo_bytes(88) // вектор инициализации
    );
}


/* Будущий дешифратор ключей */
function decript($value) {
    $length = 88 - strlen($value) % 88;
    $pkcs7_pad = $value . str_repeat(chr($length), $length);

    $enc_name = openssl_encrypt(
        $pkcs7_pad, // заполненные данные
        'AES-256-CBC', // шифр и режим
        openssl_random_pseudo_bytes(32), // секретный ключ
        0, // опции (не используются)
        openssl_random_pseudo_bytes(88) // вектор инициализации
    );
}

?>