<?php
include_once(dirname(__FILE__) . '/autoloader.php');
require __DIR__ . '/vendor/autoload.php';

use jamesRUS52\TinkoffInvest\TIAccount;
use \PHPUnit\Framework\TestCase;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TISiteEnum;
use jamesRUS52\TinkoffInvest\TICurrencyEnum;
use jamesRUS52\TinkoffInvest\TIInstrument;
use jamesRUS52\TinkoffInvest\TIPortfolio;
use jamesRUS52\TinkoffInvest\TIOrder;
use jamesRUS52\TinkoffInvest\TIOperationEnum;
use jamesRUS52\TinkoffInvest\TIIntervalEnum;
use jamesRUS52\TinkoffInvest\TICandleIntervalEnum;

date_default_timezone_set("Europe/Moscow");
$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);
$settings = json_decode($USER -> settings);
$options = array(
    'cluster' => 'ap3'
);
$pusher = new Pusher\Pusher(
    '0fbea55126d60ee64bfb',
    '1ffe5ee41d6def82ca20',
    '1267579',
    $options
);

$client = '';
if($settings-> mode == 'exchange') {
    $client = new TIClient($settings-> exchange_key, TISiteEnum::EXCHANGE);
} elseif($settings-> mode == 'sandbox') {
    $client = new TIClient($settings-> sandbox_key, TISiteEnum::SANDBOX);
}
if(!$client) exit('Не установлены, либо не валидные токены');
//$client-> sbCurrencyBalance(1000,TICurrencyEnum::USD);
$client -> setIgnoreSslPeerVerification(true);
//$client -> sbRegister();
$from = new \DateTime();
$from->sub(new \DateInterval("P7D"));
$to = new \DateTime();
/*
$ordBook = $client -> getHistoryOrderBook($settings -> FIG);
$bestSell = $ordBook-> getBestPriceToSell();
echo '<pre>';
print_r($ordBook -> getLimitDown());
echo '</pre>'; */
$portfolio = $client -> getPortfolio();
//print $portfolio -> getCurrencyBalance(TICurrencyEnum::USD) .'<br>';
/* $order = $client->sendOrder($settings -> FIG, 1, TIOperationEnum::BUY);
print $order->getOrderId(); */
//print dirname(explode('?', ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])[0]) .'/index.php';
/* $stocksMarket = $client->getStocks();
$stock = $client->getInstrumentByTicker($settings -> TICKER);
$ordBook = $client -> getHistoryOrderBook($settings -> FIG);
$stocks = $portfolio-> getAllinstruments();
$stockPortfolio = false;
foreach($stocks as $key => $value) {
    if ($value-> getTicker() == $settings -> TICKER) {
        $stockPortfolio = $stocks[$key];
        break;
    }
}
$balance = $portfolio -> getCurrencyBalance(TICurrencyEnum::USD);
$order = $client -> sendOrder($settings -> FIG , 1 , TIOperationEnum :: BUY, 1 );
$orders = $client -> getOrders();
$status = $client -> getInstrumentInfo($settings -> FIG); */
//$order = $client -> sendOrder($settings -> FIG , 1 , TIOperationEnum :: BUY);
$instr = $client -> getInstrumentInfo ($settings -> FIG);
echo '<pre>';
print_r($client->getInstrumentByTicker($settings -> TICKER));
?>