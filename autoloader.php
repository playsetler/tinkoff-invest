<?

spl_autoload_register(function ($class) {
  include_once(dirname(__FILE__) . '/classes/' . strtolower(str_replace('\\', '/', $class)) . '.php');
});

?>