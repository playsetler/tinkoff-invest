<?php

include_once(dirname(__FILE__) . '/autoloader.php');

$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
<title>Tinkoff Invest</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="css/bootstrap/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/popup.css">
<link rel="stylesheet" href="css/loader.css">
</head>
<body>

<? if(!empty($USER-> login)) { ?>

    <header>
        <div class="container-xl">
            <nav class="navbar navbar-expand-md navbar-light">
                <div class="container-fluid g-0">
                    <a class="navbar-brand" href="/"><img src="img/logo.png">Tinkoff<br>Invest</a>
                    <div class="balance col-auto col-md-2 order-1 order-md-2">Баланс: <span>
                        <div class="cssload-container">
                            <div class="cssload-shaft1"></div>
                            <div class="cssload-shaft2"></div>
                            <div class="cssload-shaft3"></div>
                        </div>
                    </span></div>
                    <button class="navbar-toggler order-2 order-md-1" type="button" data-bs-toggle="collapse" data-bs-target="#menu-header" aria-controls="menu-header" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div id="menu-header" class="collapse navbar-collapse menu">
                        <ul class="navbar-nav">
                            <li><a href="index.php" class="nav-link">Торговля</a></li>
                            <li><a href="account.php" class="nav-link">Кабинет</a></li>
                        </ul>
                    </div>
                </div>
                
            </nav>
        </div>
    </header>
    <main>
        <div class="container-xl">
            <div class="stock-info row g-0">
                <form class="row g-0 align-items-center" id="form-search-stock" method="POST" action="server.php">
                    <div class="search-stock col-4">
                        <div class="input-group">
                            <span class="input-group-text" id="status"></span>
                            <input type="text" name="ticker" class="form-control" id="ticker">
                            <!--<div class="invalid-tooltip" id="ticker-invalid">Не корректный тикер акции, пожалуйста, воспользуйтесь вариантами из выпадающего списка</div>-->
                        </div>
                        
                        <div class="matches-output">
                            <ul class="matches"></ul>
                        </div>
                    </div>
                    <div class="col-8 row g-0 align-items-center">
                        <div class="name col">
                            <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                        </div>
                        <div class="price col">
                            <span class="value">
                                <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                            </span>
                            <span class="ratio">
                                
                            </span>
                        </div>
                    </div>
                </form>
                
            </div>
            
            <div class="row g-0">
                <div class="portfolio col-12 col-md-8 order-1 order-md-2 row g-0 align-self-start">
                    <!--<h6>В портфеле:</h6>
                    <div class="dinamic">
                        <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                    </div>-->
                    <div class="summary">

                        <div class="table">
                            <!--<h6>Сводная информация: </h6>-->
                            <div class="header">
                                <div class="portfolio-count">кол-во</div>
                                <div class="portfolio-middle-price">средняя</div>
                                <div class="portfolio-total-price">сумма</div>
                                <div class="portfolio-available">доступно</div>
                            </div>
                            <div class="value">
                                <span class="count">
                                    <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                                </span>
                                <span class="middle-price">
                                    <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                                </span>
                                <span class="total-price">
                                    <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                                </span>
                                <span class="available">
                                    <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                                </span>
                            </div>
                        </div>

                        
                    </div>
                    <div class="operation row g-0">
                        <div class="count col-4">
                            <div class="sell input-group">
                                <input type="number" class="form-control" placeholder="1">
                                <div class="invalid-tooltip" id="count-sell-invalid"></div>
                                <span class="input-group-text">шт.</span>
                            </div>
                            <div class="buy input-group">
                                <input type="number" class="form-control" placeholder="1">
                                <div class="invalid-tooltip" id="count-buy-invalid"></div>
                                <span class="input-group-text">шт.</span>
                            </div>
                        </div>
                        <div class="price col-5">
                            <div class="sell input-group">
                                <input type="number" class="form-control" oninput="inputPriceSellChange(this)">
                                <div class="invalid-tooltip" id="price-buy-invalid"></div>
                                <span class="input-group-text">&nbsp;</span>
                            </div>
                            <div class="buy input-group">
                                <input type="number" class="form-control" oninput="inputPriceBuyChange(this)">
                                <div class="invalid-tooltip" id="price-buy-invalid"></div>
                                <span class="input-group-text">&nbsp;</span>
                            </div>
                        </div>
                        <div class="action col-3">
                            <div class="sell input-group">
                                <button class="animate-click" data-action="sell">продать</button>
                            </div>
                            <div class="buy input-group">
                                <button class="animate-click" data-action="buy">купить</button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="orders row g-0">
                        <h6>Заявки:</h6>
                        <div class="items">
                            <div class="order row g-0 col-12" id="order1">
                                <span class="type buy col-1"><b>П</b></span>
                                <div class="count input-group col">
                                    <input type="number" class="form-control" readonly>
                                    <span class="input-group-text">шт.</span>
                                </div>
                                <div class="price input-group col">
                                    <input type="number" class="form-control" readonly>
                                    <span class="input-group-text">$</span>
                                </div>
                                <div class="operation col-4 row g-0">
                                    <div class="edit col">
                                        <button class="animate-click" data-action="edit" data-id="1">ред.</button>
                                    </div>
                                    <div class="delete col-auto">
                                        <button class="animate-click" data-action="delete" data-id="1">X</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="history col-12 col-md-4 order-2 order-md-1">
                    <h6 class="">История: </h6>
                    <div class="items">
                        <div class="cssload-container"><div class="cssload-shaft1"></div><div class="cssload-shaft2"></div><div class="cssload-shaft3"></div></div>
                    </div>
                </div>
            </div>
            <!--
            <div class="tokens row g-0">
                <h6>Сменить токен:</h6>
                <form class="row g-0" method="POST" action="server.php">
                    <input type="hidden" name="action" value="select-token">
                    <div class="col-12">
                        <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                        <select class="form-select" id="inlineFormSelectPref">
                        <option selected>Choose...</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        </select>
                    </div>
                </form>
            </div>-->
        </div>
    </main>
    <script src="https://js.pusher.com/7.0.3/pusher.min.js"></script>
    <script src="js/popup.js"></script>
    <script src="js/tinkoff-client.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/script.js"></script>
<? } else { ?>

    <main>
        <div class="container-xl">
            <form method="POST" action="server.php">
            <h1>Вход</h1>
                <div class="mb-3">
                    <label for="login" class="form-label">Логин</label>
                    <input type="text" class="form-control" name="login" id="login" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Пароль</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <input type="hidden" name="action" value="login">
                <input type="submit" class="button">
            </form>
        </div>
    </main>

<? } ?>

<script src="js/bootstrap/bootstrap.min.js"></script>

</body>
</html>