<?php

ini_set('error_reporting', true);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

include_once(dirname(__FILE__) . '/autoloader.php');
require __DIR__ . '/vendor/autoload.php';

use jamesRUS52\TinkoffInvest\TIAccount;
use \PHPUnit\Framework\TestCase;
use jamesRUS52\TinkoffInvest\TIClient;
use jamesRUS52\TinkoffInvest\TISiteEnum;
use jamesRUS52\TinkoffInvest\TICurrencyEnum;
use jamesRUS52\TinkoffInvest\TIInstrument;
use jamesRUS52\TinkoffInvest\TIPortfolio;
use jamesRUS52\TinkoffInvest\TIOrder;
use jamesRUS52\TinkoffInvest\TIOperationEnum;
use jamesRUS52\TinkoffInvest\TIIntervalEnum;

date_default_timezone_set("Europe/Moscow");
$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);
$settings = json_decode($USER -> settings);
$pusher = new Pusher\Pusher(
    '0fbea55126d60ee64bfb',
    '1ffe5ee41d6def82ca20',
    '1267579',
    array(
        'cluster' => 'ap3'
    )
);

$client = new TIClient(GlobalEnum::TOKEN, TISiteEnum::SANDBOX);
$client -> setIgnoreSslPeerVerification(true);
$client -> sbRegister();
$from = new \DateTime();
$from -> sub(new \DateInterval("P7D"));
$to = new \DateTime();

function returnData($forced = false) {
    GLOBAL $DB, $client, $settings;
    $data = getData();

    $result = $DB -> select('history', '*', false, 'id DESC LIMIT 1');
    $lastItem = array();
    if($result) $lastItem = mysqli_fetch_assoc($result);
    if(!$forced && $lastItem['price'] == $data['price']) return false; // Если новое значение равно последнему значению из базы, то не сохраняем новое значение
    
    // Вставляем запись со стоимостью акции в базу данных
    if(!$forced) $DB-> insert('history', array(
                                                'stock' => $settings -> TICKER,
                                                'currency' => '$',
                                                'price' => $data['price']));
    
    // Получить историю стоимости акции
    $result = $DB -> select('history', '*', 'stock="'. $settings -> TICKER .'"', 'id DESC LIMIT 20');
    $historyItems = array();
    while($historyItem = mysqli_fetch_assoc($result)) {
        $historyItems[] = array(
            'currency' => $historyItem['currency'],
            'price' => $historyItem['price'],
            'time' => date('H:i:s', strtotime($historyItem['time']))
        );
    }

    // Если кол-во записей в базе данных превысило 5000 записей, то удаляем одну старую запись
    $count = $DB -> count('history');
    if(!$forced && $count > 20) $DB -> query('DELETE FROM history ORDER BY id ASC LIMIT 1');
    return array(
        'name' => $settings -> TICKER,
        'balance' => $data['balance'],
        'price' => $data['price'],
        'middlePrice' => $data['middlePrice'],
        'totalPrice' => $data['totalPrice'],
        'dinamic' => $data['dinamic'],
        'count' => $data['count'],
        'available' => $data['available'],
        'history' => json_encode($historyItems),
        'min' => $data['min'],
        'max' => $data['max']
    );
}

function getData() {
    GLOBAL $client, $settings;
    $portfolio = $client -> getPortfolio();
    $stocks = $portfolio -> getAllinstruments();
    $stock = false;
    foreach($stocks as $key => $value) {
        if ($value-> getTicker() == $settings -> TICKER) {
            $stock = $stocks[$key];
            break;
        }
    }
    $orderBook = $client -> getHistoryOrderBook($settings -> FIG);
    $balance = $portfolio -> getCurrencyBalance(TICurrencyEnum::USD);
    $price = $orderBook-> getLastPrice();
    $middlePrice = $stock-> getAveragePositionPrice();

    return array(
        'name' => $settings -> TICKER,
        'balance' => $balance,
        'price' => $price,
        'middlePrice' => $middlePrice,
        'totalPrice' => $middlePrice * $stock-> getLots(),
        'dinamic' => ($price / $middlePrice) * 100 . '%',
        'count' => $stock-> getLots() . 'шт.',
        'available' => floor($balance / $price),
        'min' => $orderBook -> getLimitDown(),
        'max' => $orderBook -> getLimitUp()
    );
}

function getStocks() {
    GLOBAL $client;
    $stockes = $client -> getStocks();
    $tickers = array();
    foreach($stockes as $stock) {
        $tickers[] = array('ticker' => $stock-> getTicker(), 'fig' => $stock -> getFigi());
    }
    return $tickers;
}

switch ($_REQUEST['action']) {
    case 'login':
        // будущая авторизация на стороне сервера
        //echo json_encode($_COOKIE['SESSID']);
        header('Location:'. dirname(explode('?', ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])[0]) .'/index.php');
        break;

    case 'get-trade-status':
        // получить статус трейдинга
        $ordBook = $client -> getHistoryOrderBook($settings -> FIG);
        $response = array();
        if($ordBook -> getTradeStatus() == 'NormalTrading') {
            $response = array('status' => 'success');
        } else {
            $response = array('status' => 'error', 'message' => 'NotAvailableForTrading');
        }
        
        $pusher -> trigger($settings -> TICKER, 'get-stock-value', returnData(true));
        $response = array_merge($response, getData(), array('name' => $settings -> TICKER));
        echo json_encode($response);
        break;

    case 'get-stocks':
        // отправить список названий всех тикеров
        echo json_encode(getStocks());
        break;

    case 'stock-select':
        // выбрать акцию для наблюдения
        $stockes = getStocks();
        $matchKey = false;
        foreach($stockes as $key => $value) {
            if($value['ticker'] == $_REQUEST['ticker']) {
                $matchKey = $key;
                break;
            }
        }

        if ($matchKey) {
            $settings = json_encode(array('FIG' => $stockes[$matchKey]['fig'], 'TICKER' => $_REQUEST['ticker']));
            $result = $DB -> update('users', array('settings' => $settings), 'login = "'. $USER -> login .'"');
            if($result) echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
        
        break;

    case 'buy':
        $client -> sendOrder($settings -> FIG, $_REQUEST['count'], TIOperationEnum::BUY, (float)$_REQUEST['price']);
        echo json_encode(getData());
        break;

    case 'sell':
        $client -> sendOrder($settings -> FIG, $_REQUEST['count'], TIOperationEnum::SELL, (float)$_REQUEST['price']);
        echo json_encode(getData());
        break;

    case 'update':
        $pusher -> trigger($settings -> TICKER, 'get-stock-value', returnData(true));
        break;

    case 'get-stock-value':
        $i = 0;
        while ($i < 20) {
            $return = returnData();
            if($return['name'] != $settings -> TICKER) break;
            if($return) $pusher -> trigger($settings -> TICKER, 'get-stock-value', $return);
            $i++;
            sleep(1);
        }
        $pusher -> trigger($settings -> TICKER, 'restart-script-event', true);
        echo '{"status":"success","message":"get-stock-value"}';
        break;
}



?>