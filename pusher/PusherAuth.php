
<?php

include_once('../autoloader.php');
require '../vendor/autoload.php';

$socket_id = $_POST['socket_id'];
$channel_name = $_POST['channel_name'];
 
//You should put code here that makes sure this person has access to this channel
/*
if( $user->hasAccessTo($channel_name) == false ) {
    header(», true, 403);
    echo( «Not authorized» );
    exit();
}
*/

$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);
 
$pusher = new Pusher\Pusher(
    '0fbea55126d60ee64bfb',
    '1ffe5ee41d6def82ca20',
    '1267579',
    array(
        'cluster' => 'ap3'
    )
);
 
//Any data you want to send about the person who is subscribing
$presence_data = array(
    'login' => $USER-> login
);
 
echo $pusher->presence_auth(
    $channel_name, //the name of the channel the user is subscribing to
    $socket_id, //the socket id received from the Pusher client library
    $USER-> id, //a UNIQUE USER ID which identifies the user
    $presence_data //the data about the person
);
exit();
?>