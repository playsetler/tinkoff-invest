<?php

include_once(dirname(__FILE__) . '/autoloader.php');

$DB = new DB(GlobalEnum::HOST, GlobalEnum::USER, GlobalEnum::PASSWORD, GlobalEnum::DBNAME);
$USER = new USER();
$SESSION = new SESSION($DB, $USER);
$settings = json_decode($USER-> settings, true);//print_r($settings['mode'] . ' ' . $settings['exchange_key']);
if(!empty($settings['exchange_key'])) $exchange_key = '*****************************' . substr($settings['exchange_key'], -4);
if(!empty($settings['sandbox_key'])) $sandbox_key = '*****************************' . substr($settings['sandbox_key'], -4);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
<title>Tinkoff Invest</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="css/bootstrap/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/loader.css">
</head>
<body>

<? if(!empty($USER-> login)) { ?>

    <header>
        <div class="container-xl">
            <nav class="navbar navbar-expand-md navbar-light">
                <div class="container-fluid g-0">
                    <a class="navbar-brand" href="/"><img src="img/logo.png">Tinkoff<br>Invest</a>
                    <div class="balance col-auto col-md-2 order-1 order-md-2">Ваш баланс: <span>
                        <div class="cssload-container">
                            <div class="cssload-shaft1"></div>
                            <div class="cssload-shaft2"></div>
                            <div class="cssload-shaft3"></div>
                        </div>
                    </span>$</div>
                    <button class="navbar-toggler order-2 order-md-1" type="button" data-bs-toggle="collapse" data-bs-target="#menu-header" aria-controls="menu-header" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div id="menu-header" class="collapse navbar-collapse menu">
                        <ul class="navbar-nav">
                            <li><a href="index.php" class="nav-link">Торговля</a></li>
                            <li><a href="account.php" class="nav-link">Кабинет</a></li>
                        </ul>
                    </div>
                </div>
                
            </nav>
        </div>
    </header>
    <main>
        <div class="container-xl">
            <div class="keys row g-0">
                <form class="row g-0" id="keys" method="POST" action="server.php">
                    <input type="hidden" name="action" value="save-token">
                    <div class="mb-3 row">
                        <label for="exchange" class="col-sm-2 col-form-label">Рабочий токен</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <input class="form-check-input mt-0" type="radio" value="exchange" name="mode"<?
                                    if(empty($exchange_key)) {
                                        echo ' disabled';
                                    } elseif(!empty($settings['mode']) && $settings['mode'] == 'exchange') {
                                        echo ' checked';
                                    }?>>
                                </div>
                                <input type="text" class="form-control" name="exchange" id="exchange" value="<?=$settings['exchange_key'];?>">
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="sandbox" class="col-sm-2 col-form-label">Тестовый токен</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <input class="form-check-input mt-0" type="radio" value="sandbox" name="mode"<?
                                    if(empty($sandbox_key)) {
                                        echo ' disabled';
                                    } elseif(!empty($settings['mode']) && $settings['mode'] == 'sandbox') {
                                        echo ' checked';
                                    }?>>
                                </div>
                                <input type="text" class="form-control" name="sandbox" id="sandbox" value="<?=$settings['sandbox_key'];?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="animate-click">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <script src="js/functions.js"></script>
    <script src="js/account.js"></script>
<? } else { ?>

    <main>
        <div class="container-xl">
            <form method="POST" action="server.php">
            <h1>Вход</h1>
                <div class="mb-3">
                    <label for="login" class="form-label">Логин</label>
                    <input type="text" class="form-control" name="login" id="login" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Пароль</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <input type="hidden" name="action" value="login">
                <input type="submit" class="button">
            </form>
        </div>
    </main>

<? } ?>

<script src="js/bootstrap/bootstrap.min.js"></script>

</body>
</html>