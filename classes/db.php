<?

/* Небольшая оболочка над функциями для работы с базой данных */
class DB {

var $HOST; /* название хоста, чтобы получить в любой момент */
var $USER; /* имя пользователя, чтобы получить в любой момент */
var $PASS; /* пароль, чтобы получить в любой момент */
var $DBNAME; /* название базы данных */
var $link_current_db; /* ссылка на базу данных, чтобы получить в любой момент */
var $active_connection = false; /* имеется ли подключение? */
var $init = false; /* отображает готовность для работы с базой данных */
var $error; /* ошибки, накопленные во время выполнения скрипта */
static $configFile = 'db.config'; /* имя файла с информацией для подключения */


/* функция-конструктор, который принимает аргументы для подключения к серверу базы данных, или если не заданы аргументы, то разбирает локальный файл db.config в котором содержаться данные для подключения */
function __construct($HOST = null, $USER = null, $PASS = '', $DBNAME = '') {

$param;

if($HOST != null && $USER != null)
{
	$this-> link_current_db = mysqli_connect($HOST, $USER, $PASS, $DBNAME);
	$this-> HOST = $HOST;
	$this-> USER = $USER;
	$this-> PASS = $PASS;
	$this-> DBNAME = $DBNAME;
}
else
{
	$param = self::check_config_file();
	if($param && !empty($param['HOST']) && !empty($param['USER']))
	{
		$this-> link_current_db = mysqli_connect($param['HOST'], $param['USER'], $param['PASS'], $param['DBNAME']);
		$this-> HOST = $param['HOST'];
		$this-> USER = $param['USER'];
		$this-> PASS = $param['PASS'];
		$this-> DBNAME = $param['DBNAME'];
	}
	else
	{
		$this-> error.='<div class="message_error_db">Ошибка подключения к базе данных</div><br>';
	}
}

if($this-> link_current_db)
	{
		$this-> active_connection = true;
		
		if(!empty($DBNAME))
		{
			$select = mysqli_select_db($this-> link_current_db, $DBNAME);
		}
		else
		{
			$select = mysqli_select_db($this-> link_current_db, $this-> DBNAME);
		}

		if(!$select) {
				$this-> error.='<div class="message_error_db">Ошибка выбора базы данных</div><br>';
				$this-> init = false;}
		else {
				$this-> init = true;}
	}
	else
	{
		$this-> error.='<div class="message_error_db">Ошибка подключения к базе данных</div><br>';
		$this-> link_current_db = null;
		$this-> active_connection = false;
	}

}


/* функция разбора локального файла config.cfg */
public static function check_config_file()
{
	$param = array('HOST' => '', 'USER' => '', 'PASS' => '', 'DBNAME' => '');
	if (file_exists (self::$configFile) && filesize(self::$configFile) > 0) {
		
		$fo = fopen(self::$configFile, "r");
		$content = fread($fo, filesize(self::$configFile));
		fclose($fo);
		
		if(preg_match('/\,?\s*HOST\s*=\s*([\w\s\.\@]*)\s*\,?/i', $content, $result)) $param['HOST'] = $result[1];
		if(preg_match('/\,?\s*USER\s*=\s*([\w\s\.\@]*)\s*\,?/i', $content, $result)) $param['USER'] = $result[1];
		if(preg_match('/\,?\s*PASS\s*=\s*([\w\s\.\@]*)\s*\,?/i', $content, $result)) $param['PASS'] = $result[1];
		if(preg_match('/\,?\s*DBNAME\s*=\s*([\w\s\.\@]*)\s*\,?/i', $content, $result)) $param['DBNAME'] = $result[1];
		
		/* если все нормально с файлом, то разбираем его и возвращаем в виде массива */
		return $param;
	}
	else
	{
		return false;
	}
}


function redirect($url) {

	header("Location:http://".$_SERVER['HTTP_HOST'].'/'.$url);

}


/* функция для смены бд, принимает аргумент с названием выбираемой базой данных */
function select_db($dbname) {

	if ($dbname != $this -> DBNAME) {
		
		$select = mysqli_select_db($this-> link_current_db, $dbname);
		if($select) {
			$this -> DBNAME = $dbname;
			return true;}
		else {
			return false;}}
	else {return true;}
	
}



/* функция проверки существования записи в бд, принимает аргументы: имя таблицы; поля, через запятую; условия выборки; порядок сортировки */
function exists($table, $field='*', $where = false, $sort = false) {

$query = 'SELECT EXISTS'.$field.' FROM '.$table;

if($where) {$query .= ' WHERE ('.$where.')';}
if($sort) {$query .= ' ORDER BY '.$sort;}

$data = mysqli_query($this-> link_current_db, $query);

if($data && mysqli_num_rows($data) != 0) {
	return mysqli_fetch_array($data);}
else {
	return false;}

}



/* произвольный запрос в базу данных */
function query($query) {

$result = mysqli_query($this-> link_current_db, $query);

if($result) {
	return true;}
else {
	return false;}

}



/* кол-во строк в определенной таблице */
function count($table, $where = false) {

	$query = 'SELECT COUNT(*) FROM '. $table;
	if($where) {$query .= ' WHERE ('.$where.')';}
	$result = mysqli_query($this-> link_current_db, $query);
	
	if($result) {
		return mysqli_fetch_array($result)[0];}
	else {
		return false;}
	
	}



/* функция выполнения запроса к бд, принимает аргументы: имя таблицы; поля, через запятую; условия выборки; порядок сортировки */
function select($table, $field = '*', $where = false, $sort = false) {

$query = 'SELECT ' . $field . ' FROM ' . $table;

if($where) {$query .= ' WHERE ('.$where.')';}
if($sort) {$query .= ' ORDER BY '.$sort;}

$data = mysqli_query($this-> link_current_db, $query);

if($data && mysqli_num_rows($data) != 0) {
	return $data;}
else {
	return false;}

}


/* функция обновления строк в бд, принимает аргументы: имя таблицы; поля, через запятую; условия выборки */
function update($table, $field, $where = false) {

$stringFields;
$arrayFields = array();
foreach($field as $key => $value)
{
	$arrayFields[] = $key . '="' . mysqli_real_escape_string($this-> link_current_db, $value) . '"';
	$stringFields = implode(',', $arrayFields);
}

$query = 'UPDATE '.$table.' SET '. $stringFields;

if($where) {$query .= ' WHERE ('.$where.')';}

$result = mysqli_query($this-> link_current_db, $query);

if($result) {
	return true;}
else {
	return false;}
}


/* функция вставки строк в бд, принимает аргументы: имя таблицы; ассоциативный массив(где соответственно ключи массива - это названия полей в бд, значения ключей массива - это значения полей в бд) либо строка вида(название=значение, название=значение)*/
function insert($table, $values, $updateFields = null) {

$fields;

$Parse_Fields = function($fields) {
	$array_fields;
	if(gettype($fields) == 'array')
	{
		foreach($fields as $key => $value)
		{
			$array_fields[$key] = '"'. mysqli_real_escape_string($this-> link_current_db, $value) .'"';
		}
	}
	elseif(gettype($fields) == 'string')
	{
		$keysvalues = explode(',', $fields);
		foreach($keysvalues as $value)
		{
			$keyvalue = explode('=', $value);
			$array_fields[$keyvalue[0]] = '"'. mysqli_real_escape_string($this-> link_current_db, $keyvalue[1]) .'"';
		}
	}
	return $array_fields;
};

$fields = $Parse_Fields($values);

$keys = implode(',', array_keys($fields));
$values = implode(',', array_values($fields));
$query = "INSERT INTO ". $table ." ($keys) VALUES ($values)";

if($updateFields != null) {
	$stringFields;
	$arrayFields = array();
	foreach($updateFields as $key => $value)
	{
		$arrayFields[] = $key . '="' . $value . '"';
	}
	$stringFields = implode(',', $arrayFields);
	$query = $query . " ON DUPLICATE KEY UPDATE $stringFields";
}

$result = mysqli_query($this-> link_current_db, $query);

if($result) {
	return true;}
else {
	return false;}
}



/* функция удаления строк в бд, принимает аргументы: имя таблицы; условия выборки */
function delete($table, $where) {

$query = 'DELETE FROM '.$table.' WHERE ('.$where.')';

$result = mysqli_query($this-> link_current_db, $query);

if($result) {
	return true;}
else {
	return false;}

}



/* функция проверки существования таблицы в бд, принимает аргумент: имя таблицы */
function checkTable($tablename)
{
	$table_list = mysqli_query($this-> link_current_db, 'SHOW TABLES FROM `'.$dbname.'`');
    while ($row = mysqli_fetch_row($table_list)) {
        if ($tablename == $row[0]) {
            return true;
        }
    }
    return false;
}

}

?>