<?

/* Класс для работы с сессиями */

class SESSION {

private $user; /* получим доступ к пользователю */
private $db; /* получим доступ к базе данных */
private $live_cookie_session; /* Время жизни куки сессии */
private $time; /* зафиксируем текущее время чтобы оно оставалось неизменным на протяжении работы всего скрипта */
private $sessid; /* идентификатор сессии */
private $ip;
private $browser;

/* Конструктор, принимает обязательные аргументы: объект DB, объект USER */
function __construct($db, $user) {

  $this-> user = $user;
  $this-> db = $db;
  $this-> time = time();
  $this-> live_cookie_session = $this-> time + 60 * 60 * 24 * 365; // (1 год)
  $this-> ip = mysqli_escape_string($this-> db-> link_current_db, strip_tags($_SERVER['REMOTE_ADDR']));
  $this-> browser = mysqli_escape_string($this-> db-> link_current_db, strip_tags($_SERVER['HTTP_USER_AGENT']));
  
  if($this-> db-> init) {
    $this-> removeSessions();
    if(!empty($_REQUEST['login']) && !empty($_REQUEST['password'])) {
      /* проверяем существование переменных login И password */
      $user = $this-> login(mysqli_escape_string($this-> db-> link_current_db, strip_tags($_REQUEST['login'])), mysqli_escape_string($this-> db-> link_current_db, strip_tags($_REQUEST['password'])));
      if($user) {
        $this-> startSession(array(
          'id'=> $user['id'],
          'login'=> $user['login'],
          'settings'=> $user['settings']));
      }
    } elseif(!empty($_COOKIE['SESSID'])) {
      /* проверяем существование куки SESSID, если не существует, то стартуем новую сессию, иначе берем данные пользователя из базы данных */
      $sessid = mysqli_escape_string($this-> db-> link_current_db, strip_tags($_COOKIE['SESSID']));
      $resultDB = $this-> db-> select('online', '*', 'sessid = "' . $sessid . '" AND ip = "' . $this-> ip . '" AND browser = "' . $this-> browser . '"');
      
      if($resultDB) {
        $this-> sessid = $sessid;
        $online_user = mysqli_fetch_assoc($resultDB);
        $resultDB = $this-> db-> select('users', '*', 'login = "' . $online_user['login'] . '"');
        if($resultDB) {
          $user = mysqli_fetch_assoc($resultDB);
          $this-> user-> toArray(array(
            'sessid'=> $online_user['sessid'],
            'id'=> $user['id'],
            'login'=> $user['login'],
            'settings'=> $user['settings']));
        }
      }
    }
  }
}


function login($login, $password) {
  $resultLogin = $this-> db-> select('users', '*', 'login = "' . $login . '"');
  if($resultLogin) {
    $user = mysqli_fetch_assoc($resultLogin);
    if($user['password'] == md5($password)) {
      return $user;
    } else return false;
  } else return false;
}

/* установка параметров пользователя по умолчанию */
function setupUserDefault() {
  $this-> user-> toArray(array(
    'login'=> 'ghost'));
}


/* функция для генерации нового идентификатора сессии */
public function generateNewSessId()
{
    return uniqid('');
}


/* функция для старта или продолжения сессии */
function startSession($user = '') {
  $this-> sessid = !empty($user['sessid']) ? $user['sessid'] : $this-> generateNewSessId();
  if(!empty($user)) {
    $this-> user-> toArray(array(
      'id'=> $user['id'],
      'login'=> $user['login'],
      'settings'=> $user['settings']));
  } else {
    $this-> setupUserDefault();
  }
  
  $result = $this-> db-> insert('online', array(
    'sessid'=> $this-> sessid,
    'login'=> $this-> user-> login,
    'ip'=> $this-> ip,
    'browser'=> $this-> browser), array(
      'sessid'=> $this-> sessid,
      'login'=> $this-> user-> login,
      'ip'=> $this-> ip,
      'browser'=> $this-> browser));
  if($result) setcookie('SESSID', $this-> sessid, $this-> live_cookie_session, '/');
}


/* функция уничтожения сессии */
function endSession() {
  $result = $this-> db-> delete('online', 'sessid = "' . $this-> sessid . '"');
  if($result) setcookie('SESSID', '', $this-> time - 3600, '/');
}

/* функция удаления устаревших сессий */
function removeSessions() {
  $this-> db -> delete('online', 'time < (NOW() - INTERVAL 6 DAY)');
}

}

?>