<?

/* класс для оперирования данными пользователя */

class USER
{
    public $id; /* логин пользователя */
    public $login; /* логин пользователя */
    public $settings; /* какая-либо информация пользователя */

    public function __construct($id = '', $login = '', $settings = '') {
        $this-> id = $id;
        $this-> login = $login;
        $this-> settings = $settings;
    }

    public function getArray() {
        return array(
            'id'=> $this-> id,
            'login'=> $this-> login,
            'settings'=> $this-> settings);
    }


    public function toArray($info_array) {
        $this-> id = $info_array['id'];
        $this-> login = $info_array['login'];
        $this-> settings = $info_array['settings'];
    }

}

?>